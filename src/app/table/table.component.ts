import { ArticleService } from './../article.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  name: string="Hello";
  data: any;
  image: string="https://static.remove.bg/remove-bg-web/2a274ebbb5879d870a69caae33d94388a88e0e35/assets/start_remove-79a4598a05a77ca999df1dcb434160994b6fde2c3e9101984fb1be0f16d0a74e.png";

  constructor(private _service: ArticleService) { }

  ngOnInit(){
    this._service.getAll().subscribe(res=>{
      this.data=res;
    });
  }

  changetext(){
    alert("1");
  }

}
